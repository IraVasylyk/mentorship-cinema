package epam.mentorship.orm.epam.mentorship.orm.service;

import epam.mentorship.orm.dto.ProducerDto;
import epam.mentorship.orm.model.Producer;

import java.util.List;
import java.util.Map;

public interface ProducerService {
    ProducerDto getProducerByPk(String name, String surname);

    List<ProducerDto> getAll();

    void save(ProducerDto producerDto);

    Map<ProducerDto,Long> getProducerAndMovieCount();
}
