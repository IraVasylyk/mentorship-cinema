package epam.mentorship.orm;

import epam.mentorship.orm.dto.ProducerDto;
import epam.mentorship.orm.epam.mentorship.orm.service.ProducerService;
import epam.mentorship.orm.model.NameSurnameClassPk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api.orm/producers")
public class ProducerController {

    private ProducerService producerService;

    @Autowired
    public ProducerController(ProducerService producerService) {
        this.producerService = producerService;
    }

    @PostMapping
    public ResponseEntity save(@RequestBody ProducerDto producerDto) {
        producerService.save(producerDto);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity getAll() {
        return ResponseEntity.ok().body(producerService.getAll());
    }

    @GetMapping("/who")
    public ResponseEntity getProducer(@RequestParam("name") String name, @RequestParam("surname") String surname) {
        NameSurnameClassPk pk = new NameSurnameClassPk();
        pk.setName(name);
        pk.setSurname(surname);
        return ResponseEntity.ok(producerService.getProducerByPk(name, surname));
    }

    @GetMapping("/count/byMovie")
    public ResponseEntity getProducerMovieCount() {

        return ResponseEntity.ok(producerService.getProducerAndMovieCount());
    }
}
