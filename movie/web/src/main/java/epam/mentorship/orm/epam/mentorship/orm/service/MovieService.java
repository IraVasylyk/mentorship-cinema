package epam.mentorship.orm.epam.mentorship.orm.service;

import epam.mentorship.orm.dto.MovieDto;

import java.util.List;

public interface MovieService {
    void save(MovieDto MovieDto);
    List<MovieDto> getAllMovies();
}
