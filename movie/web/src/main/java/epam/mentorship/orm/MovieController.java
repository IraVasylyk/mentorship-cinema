package epam.mentorship.orm;

import epam.mentorship.orm.dto.MovieDto;
import epam.mentorship.orm.epam.mentorship.orm.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/api.orm/movies")
@Transactional
public class MovieController {

    private MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping
    public ResponseEntity getMovie() {
        List<MovieDto> allMovies = movieService.getAllMovies();
        return ResponseEntity.ok(allMovies);
    }

    @PostMapping
    public ResponseEntity saveMovie(@RequestBody MovieDto movie) {
        movieService.save(movie);
        return ResponseEntity.ok().build();
    }

}
