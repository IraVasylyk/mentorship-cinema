package epam.mentorship.orm.epam.mentorship.orm.service;

import epam.mentorship.orm.dto.ProducerDto;
import epam.mentorship.orm.dto.mapper.ProducerMapper;
import epam.mentorship.orm.model.NameSurnameClassPk;
import epam.mentorship.orm.model.Producer;
import epam.mentorship.orm.repository.ProducerRepository;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProducerServiceImpl implements ProducerService {
    ProducerRepository repository;
    ProducerMapper producerMapper;

    @Autowired
    public ProducerServiceImpl(ProducerRepository repository, ProducerMapper producerMapper) {
        this.repository = repository;
        this.producerMapper = producerMapper;
    }


    public void save(ProducerDto producerDto) {
        repository.saveProducer(producerMapper.mapToEntity(producerDto));
    }


    public List<ProducerDto> getAll() {
        return repository.getAllProducers().stream().map(producerMapper::mapToDto).collect(Collectors.toList());
    }

    public ProducerDto getProducerByPk(String name, String surname) {
        NameSurnameClassPk pk = new NameSurnameClassPk();
        pk.setName(name);
        pk.setSurname(surname);
        Producer producerByPk = repository.getProducerByPk(pk);
        Hibernate.initialize(producerByPk.getMovies());
        return producerMapper.mapToDtoWithFk(producerByPk);
    }

    public Map<ProducerDto,Long> getProducerAndMovieCount(){
        Map<ProducerDto,Long> producerDtoCountMovies=new HashMap<>();
        repository.countMovieByProducer().forEach((prod,count)->producerDtoCountMovies.put(producerMapper.mapToDto(prod),count));
        return producerDtoCountMovies;
    }
}
