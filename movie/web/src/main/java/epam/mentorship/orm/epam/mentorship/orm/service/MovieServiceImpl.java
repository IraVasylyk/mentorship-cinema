package epam.mentorship.orm.epam.mentorship.orm.service;

import epam.mentorship.orm.dto.MovieDto;
import epam.mentorship.orm.dto.mapper.MovieMapper;
import epam.mentorship.orm.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class MovieServiceImpl implements MovieService {
    private MovieRepository movieRepository;
    private MovieMapper movieMapper;

    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository, MovieMapper movieMapper) {
        this.movieRepository = movieRepository;
        this.movieMapper = movieMapper;
    }

    @Override
    public void save(MovieDto movieDto) {
        movieRepository.saveMovie(movieMapper.mapToEntity(movieDto));
    }

    @Override
    public List<MovieDto> getAllMovies() {
        return movieRepository.getAllMovies().stream().map(movieMapper::mapToDto).collect(Collectors.toList());
    }
}
