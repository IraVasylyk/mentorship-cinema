package epam.mentorship.orm.dto.mapper;

import epam.mentorship.orm.dto.MovieDto;
import epam.mentorship.orm.model.Movie;
import epam.mentorship.orm.model.MovieShowDetails;
import org.springframework.stereotype.Component;

@Component
public class MovieMapper {

    public Movie mapToEntity(MovieDto movieDto){
        Movie movie=new Movie();
        movie.setCountry(movieDto.getCountry());
        movie.setName(movieDto.getName());
        movie.setProducer(movieDto.getProducer());
        MovieShowDetails details=new MovieShowDetails();
        details.setEndDay(movieDto.getEndDay());
        details.setPrice(movieDto.getPrice());
        details.setStartDay(movieDto.getStartDay());
        movie.setMovieShowDetails(details);
        return movie;
    }

    public MovieDto mapToDto(Movie movie){
        MovieDto movieDto=new MovieDto();
        movieDto.setCountry(movie.getCountry());
        movieDto.setName(movie.getName());
        movieDto.setDuration(movie.getDuration());
       // movieDto.setProducer(movie.getProducer());
        movieDto.setPrice(movie.getMovieShowDetails().getPrice());
        movieDto.setStartDay(movie.getMovieShowDetails().getStartDay());
        movieDto.setEndDay(movie.getMovieShowDetails().getEndDay());

        return movieDto;
    }
}
