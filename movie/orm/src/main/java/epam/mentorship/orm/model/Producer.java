package epam.mentorship.orm.model;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@IdClass(NameSurnameClassPk.class)
public class Producer {
    @Id
    private String name;
    @Id
    private String surname;
    @Embedded
    private PersonDetails personDetails;


    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY, mappedBy = "producer")

    private List<Movie> movies;
}
