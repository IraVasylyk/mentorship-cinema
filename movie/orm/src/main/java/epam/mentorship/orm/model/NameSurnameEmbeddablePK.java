package epam.mentorship.orm.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class NameSurnameEmbeddablePK implements Serializable {
    private String name;
    private String surname;
}
