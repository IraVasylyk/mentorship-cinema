package epam.mentorship.orm.repository;

import epam.mentorship.orm.model.Movie;
import epam.mentorship.orm.model.NameSurnameClassPk;
import epam.mentorship.orm.model.Producer;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProducerRepository {
    private static final String HQL_GET_ALL_PRODUCER_ORDER_BY_SURNAME = "From Producer m order by m.surname";


    @Autowired
    private SessionFactory sessionFactory;

    public List<Producer> getAllProducers() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(HQL_GET_ALL_PRODUCER_ORDER_BY_SURNAME).list();
    }

    public Producer getProducerByPk(NameSurnameClassPk pk) {
        Session session = sessionFactory.getCurrentSession();
        Producer producer = session.get(Producer.class, pk);

        return producer;
    }

    public void saveProducer(Producer producer) {
        Session session = sessionFactory.getCurrentSession();
        session.save(producer);
    }

    public Map<Producer, Long> countMovieByProducer() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteria = criteriaBuilder.createTupleQuery();
        Root<Movie> root = criteria.from(Movie.class);
        Join<Movie, Producer> join = root.join("producer", JoinType.LEFT);
        criteria.groupBy(root.get("producer"));
        criteria.multiselect(root.get("producer"), criteriaBuilder.count(root));

        List<Tuple> tuples = session.createQuery(criteria).getResultList();
        Map<Producer, Long> producerLongMap = new HashMap<>();
        for (Tuple tuple : tuples) {
            Producer prod = tuple.get("producer", Producer.class);
            Long count = tuple.get(1, Long.class);
            producerLongMap.put(prod, count);
        }

        return producerLongMap;
    }


}
