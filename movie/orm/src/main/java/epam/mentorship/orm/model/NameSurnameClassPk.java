package epam.mentorship.orm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
public class NameSurnameClassPk implements Serializable {
    private String name;
    private String surname;
}

