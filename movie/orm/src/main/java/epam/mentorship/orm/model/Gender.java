package epam.mentorship.orm.model;

public enum Gender {
    MALE, FEMALE;
}
