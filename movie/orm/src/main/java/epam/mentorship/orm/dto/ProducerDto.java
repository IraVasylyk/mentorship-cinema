package epam.mentorship.orm.dto;

import epam.mentorship.orm.model.Gender;
import epam.mentorship.orm.model.Movie;
import lombok.Data;

import java.util.List;

@Data
public class ProducerDto {

    private String name;
    private String surname;
    private Integer age;
    private Gender gender;
    private List<MovieDto> movies;

}
