package epam.mentorship.orm.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Data
@Embeddable
public class PersonDetails {

    private Integer age;

    @Enumerated(EnumType.STRING)
    private Gender gender;
}
