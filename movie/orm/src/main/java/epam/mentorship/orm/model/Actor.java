package epam.mentorship.orm.model;

import lombok.Data;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

@Data
@Entity
public class Actor{

    @EmbeddedId
    private NameSurnameEmbeddablePK nameSurnamePK;

    @Embedded
    private PersonDetails personDetails;

    @ManyToMany(mappedBy = "actors")
    private List<Movie> movies;
}


