package epam.mentorship.orm.dto.mapper;

import epam.mentorship.orm.dto.ProducerDto;
import epam.mentorship.orm.model.PersonDetails;
import epam.mentorship.orm.model.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.stream.Collectors;

@Component
public class ProducerMapper {

    private MovieMapper movieMapper;

    @Autowired
    public ProducerMapper(MovieMapper movieMapper) {
        this.movieMapper = movieMapper;
    }

    public ProducerDto mapToDtoWithFk(Producer producer) {
        ProducerDto producerDto = new ProducerDto();
        producerDto.setName(producer.getName());
        producerDto.setSurname(producer.getSurname());
        producerDto.setAge(producer.getPersonDetails().getAge());
        producerDto.setGender(producer.getPersonDetails().getGender());
        producerDto.setMovies(producer.getMovies() != null ?(producer.getMovies().stream().map(movieMapper::mapToDto).collect(Collectors.toList())) : null);
        return producerDto;
    }

    public ProducerDto mapToDto(Producer producer) {
        ProducerDto producerDto = new ProducerDto();
        producerDto.setName(producer.getName());
        producerDto.setSurname(producer.getSurname());
        producerDto.setAge(producer.getPersonDetails().getAge());
        producerDto.setGender(producer.getPersonDetails().getGender());
        return producerDto;
    }

    public Producer mapToEntity(ProducerDto producerDto) {
        Producer producer = new Producer();
        producer.setName(producerDto.getName());

        producer.setSurname(producerDto.getSurname());
        PersonDetails details = new PersonDetails();
        details.setAge(producerDto.getAge());
        details.setGender(producerDto.getGender());
        producer.setPersonDetails(details);
        producer.setMovies(producer.getMovies() != null ?(producerDto.getMovies().stream().map(movieMapper::mapToEntity).collect(Collectors.toList())) : null);
        return producer;
    }
}
