package epam.mentorship.orm.repository;

import epam.mentorship.orm.model.Movie;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public class MovieRepository {

    private static final String HQL_GET_ALL_MOVIE_ORDER_BY_NAME = "from Movie m order by m.name";
    private static final String HQL_GET_MOVIE_BY_NAME = "from Movie m where m.name = :name";

    @Autowired
    private SessionFactory sessionFactory;

    public List<Movie> getAllMovies() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(HQL_GET_ALL_MOVIE_ORDER_BY_NAME).list();
    }

    public Movie getMovieByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        return (Movie) session.createQuery(HQL_GET_MOVIE_BY_NAME).setParameter("name", name).uniqueResult();
    }

    public Optional<Movie> getMovieByNameUsingNaturalId(String name) {
        Session session = sessionFactory.getCurrentSession();
        return  session.byNaturalId(Movie.class).loadOptional();
    }

    public Long saveMovie(Movie movie) {
        Session session = sessionFactory.getCurrentSession();
        session.save(movie);
        return movie.getId();
    }

    public Optional<Movie> getMovieById(Long movieId) {
        Session session = sessionFactory.getCurrentSession();
        return session.byId(Movie.class).loadOptional(movieId);
    }
}
