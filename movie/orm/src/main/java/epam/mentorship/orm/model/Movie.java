package epam.mentorship.orm.model;

import epam.mentorship.orm.converter.DurationConverter;
import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.time.Duration;
import java.util.List;


@Data
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"name"}),
        indexes = @Index(name = "idx_movie_name", columnList = "name"))
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column( nullable = false)
    @NaturalId
    private String name;

    @Convert(converter = DurationConverter.class)
    private Duration duration;
    private String country;

    @OneToOne( cascade = CascadeType.ALL)
    private MovieShowDetails movieShowDetails;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(
            name = "Movie_Actor",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "actor_name", referencedColumnName = "name"),
                    @JoinColumn(name = "actor_surname", referencedColumnName = "surname") }
    )
    private List< Actor> actors;

    @ManyToOne(fetch = FetchType.EAGER, optional = false,cascade = CascadeType.ALL)
    @JoinColumns(value = {
            @JoinColumn(name = "prod_name", referencedColumnName = "name"),
            @JoinColumn(name = "prod_surname", referencedColumnName = "surname") } )
    private Producer producer;


}
