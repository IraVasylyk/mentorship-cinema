package epam.mentorship.orm.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import epam.mentorship.orm.dto.serializer.DurationSerializer;
import epam.mentorship.orm.dto.serializer.LocalDateSerializer;
import epam.mentorship.orm.model.Actor;
import epam.mentorship.orm.model.Producer;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

@Data
public class MovieDto {


    private String name;

    @JsonSerialize(using = DurationSerializer.class)
    private Duration duration;
    private String country;


    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate startDay;
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate endDay;
    private BigDecimal price;


    private List<Actor> actors;


    private Producer producer;

}
